﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class ViewLogs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<XLog> Xlogs = (List<XLog>)Application.Get("XLogs");

            lblLogDump.Text= JsonConvert.SerializeObject(Xlogs);
        }
    }
}