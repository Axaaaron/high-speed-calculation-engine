﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class TestTester : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            List<XLObj> XLObjs = (List<XLObj>)Application.Get("XLObjs");
            List<XLog> Xlogs = (List<XLog>)Application.Get("XLogs");
            HttpPostedFile hpf = Request.Files[0];
            //check file was submitted
            if (hpf != null && hpf.ContentLength > 0)
            {
                XLTest XLTR = new XLTest(Helper.ReadFully(hpf.InputStream, 0));
                XLTR.FileName = Regex.Match(hpf.FileName, @"(/|\\)?(?<fileName>[^(/|\\)]+)$").Groups["fileName"].ToString(); // to trim off whole path from browsers like IE

                XLTR.ExecuteTests("TestBedUpload", "POC", XLObjs, Xlogs);

                Response.ContentType = XLTR.MimeType;
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + XLTR.FileName + "\"");
                Response.OutputStream.Write(XLTR.Data, 0, XLTR.Data.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
                
            return;
        }
    }
}