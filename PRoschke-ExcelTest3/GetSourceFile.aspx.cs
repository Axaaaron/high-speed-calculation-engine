﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class GetSourceFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<XLObj> XLObjs = (List<XLObj>)Application.Get("XLObjs");
            XLObj XLO = null;
            string requestedHash = null;
            if (Page.RouteData.Values["hash"] != null) {
                requestedHash = Page.RouteData.Values["hash"].ToString(); }
            else
            {
                lblMessage.Text = "Please provide a Hash in the URL format ~/{Hash}/GetSourceFile";
                return;
            }
            XLO = XLObjs.Find(x => x.Hash == requestedHash);
            if (XLO != null)
            {
                // For security reasons don't send back files that weren't loaded from disk
                if (!XLO.WasLoadedFromFile)
                {
                    lblMessage.Text = "For security reasons source files that uploaded by users can't be downloaded, only tested.";
                    return;
                }

                Response.ContentType = XLO.MimeType;
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + XLO.FileName + "\"");
                Response.OutputStream.Write(XLO.Data, 0, XLO.Data.Length);
                Response.OutputStream.Flush();
                Response.End();
                return;
            }
            else
            {
                lblMessage.Text = "Excel file with the requested MD5 hash" + requestedHash + " has not been uploaded to memory or placed in the disk cache.";
            }

        }
    }
}