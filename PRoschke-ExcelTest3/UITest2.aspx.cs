﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace PRoschke_ExcelTest3
{
    public partial class UITest2 : System.Web.UI.Page
    {
        protected XLObj XLO;
        protected void Page_Load(object sender, EventArgs e)
        {

            // Initialize the form on first page load
            // Get ahold of cached collections of Excel objects and see if the Excel with the request hash is in the list
            List<XLObj> XLObjs = (List<XLObj>)Application.Get("XLObjs");
            string requestedHash = null;
            if (Page.RouteData.Values["hash"] != null) requestedHash = Page.RouteData.Values["hash"].ToString();
            else
            {  // No hash provided - grab the testfile for now
                lblMessage.Text = "Please provide a Hash in the URL format ~/{Hash}/UITest";
                requestedHash = XLObjs.Find(x => x.FileName == "Testfilev2.xlsx").Hash;
            }
            XLO = XLObjs.Find(x => x.Hash == requestedHash);
            if (XLO == null) lblMessage.Text = "No Excel with the requested Hash has been loaded - please go to /FileUpload";
            // END SETUP Block

            lock (XLO) // Interna excel model is stateful - so we are creating a critical section here to revent threading problems.
            {


                //Parse Inputs 
                foreach (string fv in Page.Request.Form)
                {
                    if (fv.StartsWith("FXIn_"))
                    {
                        // Write it to the Excel model
                        if (XLO.Xinputs[fv.Substring(5)].HTMLDataType.Equals("checkbox"))
                        {
                            //checkbox is a pain because uncheked ones don't post.  Not a beautiful soluiont below but it works.
                            XLO.Xinputs[fv.Substring(5)].Value = Page.Request[fv].Equals("0,1") ? "TRUE" : "FALSE";
                        }
                        else
                        {
                            XLO.Xinputs[fv.Substring(5)].Value = Page.Request[fv];
                        }
                    }
                }

                // calculate and check for  errors
                XLog xl;
                xl = XLO.Calculate("Excel Engine UI Test Harness", "ManualTest");


                List<XLog> Xlogs = (List<XLog>)Application.Get("XLogs");
                Xlogs.Add(xl);

                // Truncate Log list if necessary (drop entries from front so most recent ones are kept)
                if (Xlogs.Count > 10000) Xlogs.RemoveRange(0, 1000);  // given the internal structure of C# lists... this is actually pretty efficient.


                if (xl.ErrorMessage.Length > 0) lblMessage.Text = xl.ErrorMessage;

                lblSpeed.Text = xl.CalcTime.ToString();
                lblJasonInputs.Text = JsonConvert.SerializeObject(XLO.lstXinputs);
                lblJasonOutputs.Text = JsonConvert.SerializeObject(XLO.lstXoutputs);

                // Set up the Input table UI
                String sIT = "<div class=\"body - wrap\">";
                foreach (XInput xi in XLO.lstXinputs)
                {  // Input Name, Value, Type, Description , Allowed Values, Has Validation, Error Message
                    sIT += "<tr>";
                    sIT += "<td>" + xi.Name + "</td>";  //1
                    // render value column
                    if (xi.HTMLDataType.Equals("checkbox"))  //2
                    {
                        //checkboxes are a pain in the but.  To get postback when unchecked you have to include a hidden field of same nam
                        sIT += " <td style='color:" + xi.FgColor + ";" + " background-color:" + xi.BgColor + ";'><input type='hidden' value='0' name='FXIn_" + xi.Name + "'><input value='1' name='FXIn_" + xi.Name + "' type='" + xi.HTMLDataType + "'" + (xi.Value.Equals("TRUE") ? "checked" : "") + "></td>";
                    }
                    else
                    {
                        sIT += "<td><input name='FXIn_" + xi.Name + "' size='20' step='any' type='" + xi.HTMLDataType + "' value='" + xi.Value + "' style='color:" + xi.FgColor + ";" + " background-color:" + xi.BgColor + ";'" + ((xi.ValueList.Length > 0) ? " list='XValList_" + xi.Name + "'" : "") + "></td>";
                    }
                    sIT += "<td>" + xi.DataType + "</td>"; //3
                    sIT += "<td>" + xi.Description + "</td>"; //4
                    sIT += "<td>" + xi.ValueList + "</td>"; //5
                    sIT += "<td>" + xi.ValType + "</td>"; //6
                    sIT += "<td> <font style='color: red;'>" + xi.ErrorMessage + "</font></td>"; //7
                    sIT += "</tr>\r\n";
                }
                sIT += "</div>";
                InputTableRows.Text = sIT;


                //System.IO.File.WriteAllText(@"C:\Users\aaron.yiu\WriteLines.txt", InputTableRows.Text);


                // now generate the datalists for any inputs that have them so the ui has type-ahead dropdowns.  
                // Just makes a nicer UI, but is unecessary for testing.
                String sDL = "";

                try
                {
                    foreach (XInput xi in XLO.lstXinputs)
                    {
                        if (xi.ValueList.Length > 0)
                        {
                            sDL += "<datalist id='XValList_" + xi.Name + "'>\r\n";
                            foreach (string str in xi.ValueList.Split(','))
                            {
                                sDL += "<option>" + str.Trim() + "</option>";
                            }
                            sDL += "\r\n</datalist>\r\n";
                        }
                    }
                    ValueDataLists.Text = sDL;
                }
                catch (Exception)
                {
                    ValuelistErr.Text = "<p>Failed to load value list</p>";
                }


                // Generate rows of output table
                String sOT = "";
                foreach (XOutput xo in XLO.lstXoutputs)
                {  // Input Name, Value,  Description 
                    sOT += "<tr>";
                    sOT += "<td>" + xo.Name + "</td>";
                    if (xo.SgOName.Name.StartsWith("XoutputTable_"))
                    {
                        // Generate a full table
                        string sTable = "<td><table border=1>";
                        foreach (SpreadsheetGear.IRange oR in xo.SgOName.RefersToRange.Rows)
                        {
                            sTable += "<tr>";
                            foreach (SpreadsheetGear.IRange cell in oR.Cells)
                            {
                                sTable += "<td style='color:";
                                sTable += System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(cell.Font.Color.ToArgb()));
                                sTable += ";" + " background-color:";
                                sTable += System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(cell.Interior.Color.ToArgb()));
                                sTable += ";'> " + cell.Value + "</td>";
                            }
                            sTable += "</tr>";                        }
                        sOT += sTable + "</table></td>";
                    } else
                    {
                        // just a regular output.
                        sOT += "<td style='color:" + xo.FgColor + ";" + " background-color:" + xo.BgColor + ";'> " + xo.Value + "</td>";
                    }

                    sOT += "<td>" + xo.Description + "</td>";
                    sOT += "</tr>\r\n";
                }
                OutputTableRows.Text = sOT;
                lblDebugMessage.Text = XLO.DebugMessage;

                // try some image rendering ... could be a little janky - so let's stuff this into a try catch block
                try
                {
                    SpreadsheetGear.Drawing.Image imgToRender = null;
                    // check all worksheets
                    foreach (SpreadsheetGear.IWorksheet ws in XLO.Workbook.Worksheets)
                    {
                        //for shapes
                        foreach (SpreadsheetGear.Shapes.IShape shape in ws.Shapes)
                        {
                            if (shape.HasChart)
                            {
                                imgToRender = new SpreadsheetGear.Drawing.Image(shape);
                                break;
                            }
                        }
                        if (imgToRender != null) break;
                    }

                    if (imgToRender != null)
                    {
                        //Found a chart
                        System.IO.MemoryStream strm = new System.IO.MemoryStream();
                        imgToRender.GetBitmap().Save(strm, System.Drawing.Imaging.ImageFormat.Png);
                        string base64String = Convert.ToBase64String(strm.ToArray());
                        ltrImage.Text = "<hr /> <h3>Rendering First Chart Found ... </h3><img src='data:image/png; base64," + base64String + "' />";
                    }
                }
                catch (Exception)
                {
                    // eat exceptions.... but let user know...
                    //throw;
                    ltrImage.Text = "<p>Failed to render image</p>";
                }
              
            }
      }

        public void CalculateButton_Click(Object sender, EventArgs e)
        {
            // Does nothing just added butto to invoke the postback itself... all functions covered in page load
            return;
        }
    }
    
}
