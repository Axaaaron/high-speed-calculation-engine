﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class JSONPoster : System.Web.UI.Page
    {
        protected string sText = "Jason isn't here right now...";
        protected string sMessage = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            // Initialize the form on first page load
            // Get ahold of cached collections of Excel objects and see if the Excel with the request hash is in the list
            List<XLObj> XLObjs = (List<XLObj>)Application.Get("XLObjs");
            XLObj XLO = null;
            string requestedHash = null;
            if (Page.RouteData.Values["hash"] != null) requestedHash = Page.RouteData.Values["hash"].ToString();
            else
            {  // No hash provided - grab the testfile for now
                sMessage = "Please provide a Hash in the URL format ~/{Hash}/JSONPoster";
                requestedHash = XLObjs.Find(x => x.FileName == "Testfilev2.xlsx").Hash;
            }
            XLO = XLObjs.Find(x => x.Hash == requestedHash);
            if (XLO == null)
            {
                sMessage = "No Excel with the requested Hash has been loaded - please go to /FileUpload";
                return;
            }
            // END SETUP Block

            if (IsPostBack)
            {
                List<jInput> lstJi = JsonConvert.DeserializeObject<List<jInput>>(Request.Form["JSON"].ToString());
                XInput xi = null;

                lock (XLO)  // enforce critical section for threadsafety (get exclusive lock on this XLO Exel memory instance.)
                {
                    XLO.resetToDefalutInputs();   // set inputs back to what they where when engine was uploaded.

                    // load submitted valus into XLO engine
                    foreach (jInput ji in lstJi)
                    {
                        if (XLO.Xinputs.TryGetValue(ji.Name, out xi)) xi.Value = ji.Value;
                    }

                    // run calc
                    XLog xl;
                    xl = XLO.Calculate("JSONPoster", "n/a");

                    // add result to in memory log
                    List<XLog> Xlogs = (List<XLog>)Application.Get("XLogs");
                    Xlogs.Add(xl);

                    // Truncate Log list if necessary (drop entries from front so most recent ones are kept)
                    if (Xlogs.Count > 10000) Xlogs.RemoveRange(0, 1000);  // given the internal structure of C# lists... this is actually pretty efficient.

                    Response.ContentType = "text/plain";
                    Response.Write(JsonConvert.SerializeObject(XLO.lstXoutputs));
                    Response.OutputStream.Flush();
                    Response.End(); 
                }
            }
            else
            {
                //Generate as sample based on the default values
                sText = JsonConvert.SerializeObject(XLO.lstXinputs);
            }




        }
        public class jInput
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }
    }

}