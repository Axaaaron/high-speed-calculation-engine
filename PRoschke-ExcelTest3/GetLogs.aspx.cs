﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class GetLogs : Page
    {
        XLObj XLO = null;
        List<XLObj> XLObjs = null;
        List<XLog> Xlogs = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ahold of cached collections of Excel objects and see if the Excel with the request hash is in the list

            XLObjs = (List<XLObj>)Application.Get("XLObjs");
            string logEngineFormat = null;
            if (Page.RouteData.Values["hash"] != null) logEngineFormat = Page.RouteData.Values["hash"].ToString();
            else
            {  // No hash provided - grab the testfile for now
                lblMessage.Text = "Please provide a Hash in the URL format ~/{Hash}/UITest";
                logEngineFormat= XLObjs.Find(x => x.FileName == "Testfilev2.xlsx").Hash;
            }
            XLO = XLObjs.Find(x => x.Hash == logEngineFormat);
            if (XLO == null) lblMessage.Text = "No Excel with the requested Hash has been loaded - please go to main page";

            Xlogs = (List<XLog>)Application.Get("XLogs");

            if (!IsPostBack)
            {
                tbServiceName.Text = XLO.Xname;
                tbServiceRevision.Text = XLO.Xrevision;

            }




        }
        // What's below is hardly beautiful stuff, but it will work for the POC which is limited to 10,000 log entries in memory.  
        // Connecting this to an actual logging framework will provide much of the below out of the box.
        protected void btnHTML_Click(object sender, EventArgs e)
        {
            string sLTable = "";
            // Generated log table headers
            sLTable = "<th>Time</th><th>Service Name</th><th>Service Revision</th>";
            foreach (XInput xi in XLO.lstXinputs)
            {
                sLTable += "<th>Input: " + xi.Name + "</th>";
            }
            foreach (XOutput xo in XLO.lstXoutputs)
            {
                sLTable += "<th>Output: " + xo.Name + "</th>";
            }

            sLTable += "<th>Error Message</th><th>Log Message</th></tr>\n\r";

            // Generate log table rows
            foreach (XLog xl in Xlogs)
            {
                if ((tbServiceName.Text.Length == 0 || tbServiceName.Text.Equals(xl.ServiceName)) &&
                    (tbServiceRevision.Text.Length == 0 || tbServiceRevision.Text.Equals(xl.ServiceRevision)) &&
                    (tbErrorMessageFilter.Text.Length == 0 || xl.ErrorMessage.Contains(tbErrorMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbInputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xinputs).Contains(tbInputFilter.Text)) &&
                    (tbOutputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xoutputs).Contains(tbOutputFilter.Text)))
                    // TODO: Implement JSONPath filters for inputs and oputpus
                {
                    sLTable += "<tr><td>" + xl.TimeStamp + "</td><td>" + xl.ServiceName + "</td><td>" + xl.ServiceRevision + "</td>"; ;
                    foreach (XInput xi in XLO.lstXinputs)
                    {
                        string sTry;
                        xl.Xinputs.TryGetValue(xi.Name, out sTry);
                        sLTable += "<td>" + sTry + "</td>";
                    }
                    foreach (XOutput xo in XLO.lstXoutputs)
                    {
                        string sTry;
                        xl.Xoutputs.TryGetValue(xo.Name, out sTry);
                        sLTable += "<td>" + sTry + "</td>";
                    }
                    sLTable += "<td>" + xl.ErrorMessage + "</td><td>" + xl.LogMessage + "</td></tr>\n\r" ;
                }
            }
            ltLog.Text = sLTable;
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            //  Generate filtered list of Matching logs
            List<XLog> fXL = new List<XLog>();
            foreach (XLog xl in Xlogs)
            {
                if ((tbServiceName.Text.Length == 0 || tbServiceName.Text.Equals(xl.ServiceName)) &&
                    (tbServiceRevision.Text.Length == 0 || tbServiceRevision.Text.Equals(xl.ServiceRevision)) &&
                    (tbErrorMessageFilter.Text.Length == 0 || xl.ErrorMessage.Contains(tbErrorMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbInputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xinputs).Contains(tbInputFilter.Text)) &&
                    (tbOutputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xoutputs).Contains(tbOutputFilter.Text)))
                // TODO: Implement JSONPath filters for inputs and outputs
                {
                    fXL.Add(xl);
                }
            }
            XLTest XLT = (XLTest)Application.Get("XTestResultTemplate");
            XLTest XLTR = null;
            lock (XLT)
            {
                XLTR = XLT.TemplateToOutput(XLO);
            }
            XLTR = new XLTest(XLTR.Data); // TODO: That was not nice... should find a nice way to reparse for template fields in the object itself... but it's late....

            XLTR.LoadTestCasesFromLogs(fXL);
            XLTR.UpdateMemoryFile();


            Response.ContentType = XLTR.MimeType;
            Response.AppendHeader("content-disposition", "attachment; filename=\"" + XLTR.FileName + "\"");
            Response.OutputStream.Write(XLTR.Data, 0, XLTR.Data.Length);
            Response.OutputStream.Flush();
            Response.End();
        }

        protected void btnJSON_Click(object sender, EventArgs e)
        {
            //  Generate filtered list of Matching logs
            List<XLog> fXL = new List<XLog>();
            foreach (XLog xl in Xlogs)
            {
                if ((tbServiceName.Text.Length == 0 || tbServiceName.Text.Equals(xl.ServiceName)) &&
                    (tbServiceRevision.Text.Length == 0 || tbServiceRevision.Text.Equals(xl.ServiceRevision)) &&
                    (tbErrorMessageFilter.Text.Length == 0 || xl.ErrorMessage.Contains(tbErrorMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbInputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xinputs).Contains(tbInputFilter.Text)) &&
                    (tbOutputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xoutputs).Contains(tbOutputFilter.Text)))
                // TODO: Implement JSONPath filters for inputs and outputs
                {
                    fXL.Add(xl);
                }
            }



            Response.ContentType = "text/plain";
            Response.AppendHeader("content-disposition", "attachment; filename=\"ExcelTesterJSONLogs.txt\"");
            Response.Write(JsonConvert.SerializeObject(fXL));
            Response.OutputStream.Flush();
            Response.End();
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string sLTable = "";
            // Generated log table headers
            sLTable = "<th>Time</th><th>Service Name</th><th>Service Revision</th>";
            foreach (XInput xi in XLO.lstXinputs)
            {
                sLTable += "<th>Input: " + xi.Name + "</th>";
            }
            foreach (XOutput xo in XLO.lstXoutputs)
            {
                sLTable += "<th>Output: " + xo.Name + "</th>";
            }

            sLTable += "<th>Error Message</th><th>Log Message</th></tr>\n\r";

            // Generate log table rows
            foreach (XLog xl in Xlogs)
            {
                if ((tbServiceName.Text.Length == 0 || tbServiceName.Text.Equals(xl.ServiceName)) &&
                    (tbServiceRevision.Text.Length == 0 || tbServiceRevision.Text.Equals(xl.ServiceRevision)) &&
                    (tbErrorMessageFilter.Text.Length == 0 || xl.ErrorMessage.Contains(tbErrorMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbLogMessageFilter.Text.Length == 0 || xl.LogMessage.Contains(tbLogMessageFilter.Text)) &&
                    (tbInputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xinputs).Contains(tbInputFilter.Text)) &&
                    (tbOutputFilter.Text.Length == 0 || JsonConvert.SerializeObject(xl.Xoutputs).Contains(tbOutputFilter.Text)))
                // TODO: Implement JSONPath filters for inputs and oputpus
                {
                    sLTable += "<tr><td>" + xl.TimeStamp + "</td><td>" + xl.ServiceName + "</td><td>" + xl.ServiceRevision + "</td>"; ;
                    foreach (XInput xi in XLO.lstXinputs)
                    {
                        string sTry;
                        xl.Xinputs.TryGetValue(xi.Name, out sTry);
                        sLTable += "<td>" + sTry + "</td>";
                    }
                    foreach (XOutput xo in XLO.lstXoutputs)
                    {
                        string sTry;
                        xl.Xoutputs.TryGetValue(xo.Name, out sTry);
                        sLTable += "<td>" + sTry + "</td>";
                    }
                    sLTable += "<td>" + xl.ErrorMessage + "</td><td>" + xl.LogMessage + "</td></tr>\n\r";
                }
            }
            ltLog.Text = sLTable;
        }
    }
}