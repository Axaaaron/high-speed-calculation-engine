﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class GetTestSetTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<XLObj> XLObjs = (List<XLObj>)Application.Get("XLObjs");
            XLObj XLO = null;
            string requestedHash = null;
            if (Page.RouteData.Values["hash"] != null) {
                requestedHash = Page.RouteData.Values["hash"].ToString(); }
            else
            {
                lblMessage.Text = "Please provide a Hash in the URL format ~/{Hash}/GetTestSetTemplate";
                return;
            }
            XLO = XLObjs.Find(x => x.Hash == requestedHash);
            if (XLO != null)
            {

                XLTest XLT = (XLTest)Application.Get("XTestResultTemplate");
                XLTest XLTR;
                lock (XLT)
                {
                    XLTR = XLT.TemplateToOutput(XLO);
                }
 
                Response.ContentType = XLTR.MimeType;
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + XLTR.FileName + "\"");
                Response.OutputStream.Write(XLTR.Data, 0, XLTR.Data.Length);
                Response.OutputStream.Flush();
                Response.End();
                return;
            }
            else
            {
                lblMessage.Text = "Excel Engine file with the requested MD5 hash" + requestedHash + " has not been uploaded to memory or placed in the disk cache.";
            }

        }
    }
}