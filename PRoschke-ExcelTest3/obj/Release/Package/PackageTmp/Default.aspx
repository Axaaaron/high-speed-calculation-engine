﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PRoschke_ExcelTest3._Default" %>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Rosetta Portal - Calculation</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="favicon.ico" title="axahongkong-favicon" rel="icon">
    <link rel="stylesheet" href="assets/Icon/css/fontello.css">
    <link rel="stylesheet" href="assets/foundation/css/foundation.css">
    <script src="https://use.typekit.net/rac8rmf.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="assets/js/jQueryRotate.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <body>
      <div class="body-wrap">
         <div class="box-full-width">
         <%--      <div class="box-full-width">
            <h1>Database
            </h1>
            <div class="search-wrap">
               <div class="row">
                  <div class="medium-6 columns no-padding">
                     <label>
                     <input type="text" placeholder="Search" aria-describedby="exampleHelpTex" data-abide-ignore>
                     </label>
                  </div>
                  <a href="about.html" class="button light-pink-bg"><i class="icon-search"></i></a>
                  <div class="medium-6 columns no-padding align-right">
            
                     <button id="upload_button" type="button" class="button med blue-bg"><i class="icon-upload"></i> &nbsp; Upload</button>
            <%--                  <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                     </td>
                     <td>
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Upload" />
                     </td>--%>
         <%--               </div>
            </div>
            </div>--%>
         <div class="search-wrap">
            <%--        <form>--%>
            <div class="row">
               <div class="large-4 columns no-padding">
                  <h5>
                     Please upload your <strong>Excel file to process caluclation</strong> or upload <strong>test file to run a test</strong>. 
                     If you have already created a <b>test set</b> to run you can upload it <a href="TestTester.aspx">here</a>
                  </h5>
               </div>
               <div class="large-6 columns no-padding align-right float-right">
                  <button id="upload_button" type="button" class="button large blue-bg"><i class="icon-upload"></i> &nbsp; Upload</button> 
                  <%--<button id="run_test_button" type="button" class="button large light-pink-bg"><i class="icon-dashboard"></i> &nbsp; Run Test</button>--%>
               </div>
            </div>
            <%--</form>--%>
         </div>
         <div class="serach-result-wrap">
            <h2>Latest Uploads</h2>
            <div class="table-wrap">
               <asp:GridView ID="FileGV" runat="server" AutoGenerateColumns="false" OnSelectedIndexChanged="FileGV_SelectedIndexChanged1">
                  <Columns>
                     <asp:HyperLinkField HeaderText="File Test UI" DataTextField="FileName" DataNavigateUrlFields="Hash" DataNavigateUrlFormatString="~/{0}/UITest" />
                     <asp:BoundField HeaderText="Memory Used" DataField="MemUsed" DataFormatString="{0:N0} bytes" />
                     <asp:BoundField DataField="Length" HeaderText="File Size" DataFormatString="{0:N0} bytes" />
                     <asp:BoundField DataField="Hash" HeaderText="Engine ID (MD5 Hash)" />
                     <asp:BoundField DataField="Xrevision" HeaderText="Revision" />
                     <asp:BoundField HeaderText="Persisted" DataField="WasLoadedFromFile" />
                     <asp:HyperLinkField HeaderText="Download"  DataTextField="FileName"  DataTextFormatString="Link" DataNavigateUrlFields="Hash" DataNavigateUrlFormatString="~/{0}/GetSourceFile" />
                  </Columns>
               </asp:GridView>
            </div>
         </div>
      </div>
      </div>
      <div id="popup_container" class="hide">
         <div id="overlay-background">
            <div class="pop-up-close"><i class="icon-cancel-1"></i></div>
         </div>
         <div id="pop-up-content">
            <div class="pop-up-header">
               <div class="pop-up-header-small">
                  <h1>Upload File</h1>
               </div>
            </div>
            <div class="pop-up-content">
               <div class="dragdopbox">
                  <div class="dragdroptext">
                     <div id="upload_text">
                        <h1>
                           <p>Drag and Drop</p>
                           <p>or</p>
                        </h1>
                        <p>
                           <input type="file" name="file" id="fileupload" class="inputfile inputfile-6"/>
                           <label for="fileupload">Select File</label>
                        </p>
                     </div>
                     <div id="non_upload_text" class="hide">
                        <h1>
                           <p id="file_name"></p>
                           <p>&nbsp;</p>
                        </h1>
                        <p>
                        <div id="remove_button" class="upload_link"/>Remove</div>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="button-clear-wrap">
                  <%--<button id="Button1" type="button" class="button med light-pink-bg align-center"><i class="icon-upload"></i> &nbsp; Upload</button>--%>
                  <%--<button id="upload_button" type="button" class="button med blue-bg" OnClick="Button1_Click"><i class="icon-upload"></i> &nbsp; Upload</button>--%>
                  <a hidden><asp:FileUpload ID="FileUpload1" runat="server"/></a>
                  <asp:Button ID="Button1" runat="server"  OnClick="Button1_Click" Text="Upload" />
               </div>
            </div>
         </div>
       </div>
       <script type="text/javascript" src="assets/js/jquery.min.js"></script>
       <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>
       <script type="text/javascript" src="assets/js/jQueryRotate.js"></script>
       <script type="text/javascript" src="assets/js/main.js"></script>
   </body>
</asp:Content>