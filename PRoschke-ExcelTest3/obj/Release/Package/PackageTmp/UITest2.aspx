﻿<%@ Page Title="UI Test" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="UITest2.aspx.cs" Inherits="PRoschke_ExcelTest3.UITest2" %>
<asp:Content ID="Head" ContentPlaceHolderID="head" runat="server">
    <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title>Rosetta Portal - Calculation</title>
   <link rel="stylesheet" href="assets/css/style.css">
   <link href="favicon.ico" title="axahongkong-favicon" rel="icon">
   <link rel="stylesheet" href="assets/Icon/css/fontello.css">
   <link rel="stylesheet" href="assets/foundation/css/foundation.css">
   <link rel="stylesheet" href="assets/jquery-ui/jquery-ui.css">
   <script src="https://use.typekit.net/rac8rmf.js"></script>
   <script>try{Typekit.load({ async: true });}catch(e){}</script> 
   <script type="text/javascript" src="assets/js/jquery.min.js"></script>
   <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>
   <script type="text/javascript" src="assets/js/jQueryRotate.js"></script>
   <script type="text/javascript" src="assets/js/main.js"></script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <body>
    <h2>Service Datails: <%=XLO.Xname %></h2>
    <table border="1" cellpadding="4">
        <tr>
            <td>Release Date</td><td><%= XLO.XreleaseDate.Equals("") ? "Please create and fill an 'XreleaseDate' named field in the Excel" : XLO.XreleaseDate %></td>
            <td>Author</td><td><%=XLO.Xauthor.Equals("") ? "Please create and fill an 'Xauthor' named field in the Excel" : XLO.Xauthor %></td>
        </tr>
        <tr>
            <td>Revision</td><td><%= XLO.Xrevision.Equals("") ? "Please create and fill an 'Xrevision' named field in the Excel" : XLO.Xrevision %></td>
            <td>Xtags</td><td><%= XLO.Xrevision.Equals("") ? "Please create and fill an 'Xtags' named field in the Excel (format: [tag1][tag2]...)" : XLO.Xtags %></td>

        </tr>
        <tr>
            <td>File Size</td><td><%= String.Format("{0:#,##0}", XLO.Length/1024) %> Kb</td>
            <td>File Name</td><td><%=XLO.FileName %></td>
        </tr>
        <tr>
            <td>Engine Hash</td><td><%= XLO.Hash %></td>
            <td>Memory Size</td><td><%= String.Format("{0:#,##0}", XLO.MemUsed/1024) %> Kb</td>
        </tr>
        <tr>
            <td>Notes</td><td colspan="3"><%=XLO.Xnotes %></td>

        </tr>
        <tr>
            <td colspan="2"><div style="text-align:center"><a href="/<%=XLO.Hash %>/GetSourceFile">Excel File</a></div></td>
            <td colspan="2"><div style="text-align:center"><a href="/<%=XLO.Hash %>/GetLogs">View Logs</a></div></td>
        </tr>
        <tr>
            <td colspan="2"><div style="text-align:center"><a href="/<%=XLO.Hash %>/GetTestSetTemplate">Test Template</a></div></td>        
            <td colspan="2"><div style="text-align:center"><a href="/<%=XLO.Hash %>/JSONPoster">JSON Test</a></div></td>
        </tr>

    </table> 
    <p /> <hr /> 

   
 
        <p><asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="True" Font-Names="Arial" ForeColor="#CC0000" ViewStateMode="Disabled"></asp:Label></p>
            <p />
       
        	<table cellspacing="1" rules="all" border="1" id="tblInputs">
		    <tr>
			    <th scope="col">Input Name</th>
                <th scope="col">Value</th>
                <th scope="col">Data Type</th>
                <th scope="col">Comments</th>
                <th scope="col">Allowed Values</th>
                <th scope="col">Has Validation</th>
                <th scope="col">Error Message</th>
		    </tr>
                <asp:Literal ID="InputTableRows" runat="server"></asp:Literal>
            </table>

           <asp:Literal ID="ValueDataLists" runat="server"></asp:Literal>
            <br />
            <asp:Literal ID="ValuelistErr" ViewStateMode="Disabled" runat="server"></asp:Literal>

    <div>
        <asp:Button ID="ButtonSubmit" Text="Calculate" OnClick="CalculateButton_Click" runat="server"/>
    <p></p>
          <p> <span class="newStyle1">If you have already created a test set to run you can upload it</span> <a href="/TestTester.aspx">here</a>.</p>
    

        </div>
        <hr />
        <h2>Outputs</h2>
        <div>
             
        	<table cellspacing="1" rules="all" border="1" id="tblOutputs">
		    <tr>
			    <th scope="col">Output Name</th>
                <th scope="col">Value</th>
                <th scope="col">Comments</th>

		    </tr>
                <asp:Literal ID="OutputTableRows" runat="server"></asp:Literal>
            </table>

        </div>
        <h2>Log Results</h2>
        <div>
            <table cellspacing="1" rules="all" border="1" id="tblLogResults">
        <tr>
            <td>Log Result</td><td><%=XLO.XlogMessage %></td>
            <td>Time Taken</td><td><asp:Label ID="lblSpeed" ViewStateMode="Disabled" runat="server"></asp:Label> milliseconds.</td>
        </tr>
        <tr>
            <td>Inputs</td><td colspan="3"><asp:Label ID="lblJasonInputs" ViewStateMode="Disabled" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Outputs</td><td colspan="3"><asp:Label ID="lblJasonOutputs" ViewStateMode="Disabled" runat="server"></asp:Label></td>
        </tr>
    </table> 
        </div>
         <h2>Debug Messgaes</h2>
        <div>
            <table cellspacing="1" rules="all" border="1" id="tbDebug">
        <tr>
            <td>Message</td><td><asp:Label ID="lblDebugMessage" ViewStateMode="Disabled" runat="server"></asp:Label></td>
        </tr>
 
    </table> 
        </div>



    <br />
      <asp:Literal ID="ltrImage" ViewStateMode="Disabled" runat="server"></asp:Literal>


   </body>
</asp:Content>