﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="PRoschke_ExcelTest3.Contact" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Rosetta Portal - Calculation</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="favicon.ico" title="axahongkong-favicon" rel="icon">
    <link rel="stylesheet" href="assets/Icon/css/fontello.css">
    <link rel="stylesheet" href="assets/foundation/css/foundation.css">
    <link rel="stylesheet" href="assets/jquery-ui/jquery-ui.css">
    <script src="https://use.typekit.net/rac8rmf.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script> 
</asp:Content>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <body>


    <div class="body-wrap">
      <div class="box-full-width" id="contact-form">
        <h1>Contact</h1>
        <h5>If you have any questions or more information about POC please contact us.</h5>
        <div class="wrapper top-margin-small" >
          <h5>
            <p><strong>AXA Asia Digital Group </strong>- Christophe Le Padellec <a href="mailto:chris.lepadellec@axa.com.hk"><i class="icon-mail"></i></a></p>
            <p><strong>AXA Asia Transformation Group </strong>- Peter Roschke <a href="mailto:peter.roschke@axa.com.hk"><i class="icon-mail"></i></a></p>
          </h5>
        </div>
      </div>    
    </div>

    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="assets/js/jQueryRotate.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
  </body>

</asp:Content>
