﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="PRoschke_ExcelTest3.About" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Rosetta Portal - Calculation</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="favicon.ico" title="axahongkong-favicon" rel="icon">
    <link rel="stylesheet" href="assets/Icon/css/fontello.css">
    <link rel="stylesheet" href="assets/foundation/css/foundation.css">
    <link rel="stylesheet" href="assets/jquery-ui/jquery-ui.css">
    <script src="https://use.typekit.net/rac8rmf.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script> 
</asp:Content>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
 <body>

    <div class="body-wrap" id="how-to-use">
      <div class="box-full-width">
        <h1>About</h1>
        <div class="row column top-margin-small">
          <h2>What is it?</h2>
          <div class="small-6 large-7 columns">
            <ul>
              <li>This POC allows you to <strong>turn Excel files that perform calculations into robust, scalable services</strong> that can be used for high volume regression testing or even production systems.</li>
              <li><strong>Existing Excel files can easily be adapted</strong> by creating named ranges for input fields (Xinput_InputName) and output fields or tables (Xoutput_OutputName).  If you haven’t used Excel named ranges before, you can create them by selected a cell and typing a name in the area just under the file menu and pressing enter (usually it says something like ‘A3’ there if a cell hasn’t been named yet).  You can name cells anywhere in your file and they will be extracted as long as they start with Xinput_ or Xoutput_.  Additional fields to support service management, logging and version control are also available.  These are not required but you can check them out in the example files. </li>
            </ul>
          </div>
          <div class="large-4 columns columns float-left left-margin-large">
            <img src="assets/images/ExcelSnip.png">
          </div>
        </div>
        <div class="row column top-margin-small">
          <h2>How to use?</h2>
          <ul>
            <li>After you upload your excel you will be taken to a <strong>dynamically created testing UI</strong> where you can check that everything works as well as see some statistics on the engine you just created.</li>
            <li>From this page you can also download a customized <strong>Excel test set template</strong> that will allow you to specify hundreds of test cases for your engine.</li>
            <li>These can also be uploaded along with expected output values.  This will trigger an execution of the test cases, a comparison of the expected outputs, and <strong>return you a completed regression report for the test set in Excel.</strong></li>
            <li>For integration with production systems and user interfaces, there is <strong>full support for rich input validation</strong> rules via the existing mechanics in Excel.  If you want to specify that an input must be in the list of values in a dynamically filtered column based on the value of a second input but only if it’s after 4pm CET… then all you have to do is apply that rule to the input field you specified in Excel. (Google Excel field validation … it’s quite comprehensive and that rule can be written and tested in less than a minute).  The engine will pick up the rule and log any error message you specify.  Currently errors do not block returning other values, but that is just a POC choice.</li>
            <li><strong>Error and Log messages are dynamically picked up</strong> if you specify XlogMessage and XerrorMessage fields in your Excel and update them as inputs and output field change.   As with everything else above, just test it in Excel on your desktop until you are satisfied with the behaviour and the engine manger will ensure it's properly managed.</li>
            <li>To make things easy, you can also <strong>generate regression beds directly from the log viewer.</strong>  Expected values will be pre-populated in the resulting Excel file and you can even run and compare these against different versions of your service or other services.</li>
            <li><strong>All engine calls are logged</strong> (inputs, outputs, errors, Meta information).  Logs can be searched and filtered.</li>
            <li><strong>Image rendering</strong> is also supported for many chart types.  The test UI page will attempt to render the first chart found after updating any input value.</li>
            <li>There is also a simple <strong>JSON HTTP interface</strong> that can be used by applications and volume testing tools.</li>
          </ul>
        </div>
        <div class="row column top-margin-small">
          <h2>What's Next</h2>
          <ul>
            <li>Please use this POC to evaluate the spreadsheets you use and let us know any bugs in the system.  Can they easily be made to work in this model?  How is the performance?  Can you manage the test sets more easily using this approach?</li>
            <li>From here there are three practical scenarios to consider for further development:
              <ul>
                <li>Use this as a way to drive testing for larger projects.  It’s a way to generate test cases, run them against the excel engine, get expected results and then run them against the production system being developed to prove equivalence.</li>
                <li>Use it as a way to run calculations for existing production systems.  It is easy to have browser based front-ends, even as static HTML that use these engines.</li>
                <li>Use the full Rosetta stack to directly create web UIs tied to calculations.</li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="row column top-margin-small">
          <h2>Technology</h2>
          <ul>
            <li>The application itself is straightforward.  Most of the heavy lifting is done by a single server-side .net DLL from SpreadsheetGear.  This provides an efficient in-memory Excel model that can be used to create, read, modify, and recalculate Excel files.  It's also possible to extend Excel formula functions to invoke custom server side code when the files are processed.  The licensing terms are very reasonable. </li>
            <li>Everything else is basic C# spread across a few class files and ASPXs.  Note: route tables are used so please check Global.asax for mappings.</li>
            <li>The code can be deployed on an in-house server as easily as in Azure as a simple WebApp.   Everything is in memory so no database or external services needed.  The sample flies and reporting templates are located in the ~/files directory and automatically loaded</li>
            <li>Code wise, basic encapsulation has been done and you’ll find some comments. Further development should probably start by rewriting this as a proper MVC application and using the existing solution as sample code.  Total developer time to this point has been less than one person week.</li>
          </ul>
        </div>
        <div class="row column top-margin-small">
          <h2>Extension possibilities</h2>
          <ul>
            <li><strong>Generate benefit illustrations directly from Excel</strong>. The underlying library also supports the automation of Excel graphs and charts. To the extent that we can imagine a reasonable packaging of excel charts into an illustration for a particular product, it should be relatively easy to accomplish. This could further be extended by adding it to a simple HTML to PDF reporting app.</li>
            <li>This is a subset of the Asia Digital team’s Rosetta project, which includes the ability to generate <strong>application and pricing UIs</strong> from Excel files.  This is being further developed and shows great promise as a way of supporting some of our business lines.  You can see some of the possibilities from the TestUI in this POC, which takes things like validation rules, Typeahead lists, error handling, and field descriptions directly from Excel.  Rosetta adds dynamic updates, layout control and full application environment to this, while still allowing end users to specify the UI.   (approach would be to use JSON data binding to AngularJS layouts that are dynamically generated - relying on the Excel file to deal with state and field management)</li>
            <li>In its current form it is readily possible to create <strong>reference data services</strong> via this POC (using VLOOKUP, returning full tables and various other Excel techniques).  However Excel is not that good at supporting insurance tables - for example wild card matching and conditional overrides are not easy to efficiently represent and automate.  For a good example of what should be possible look at <a href="http://openl-tablets.org/">OpenL Tablets</a>.  These types of mechanics could readily be added in a future iteration and supported both on the desktop Excel development and server runtime environment.</li>
            <li><strong>Generalized logging framework</strong>.  This POC contains the pattern for an open logging framework that is able to capture and the details (inputs, outputs, errors, source, tags, business purposes, reference IDs, etc.) of all calls to the engine.  This is closely coupled with the regression testing engine.  Logs can be searched, filtered and used to create regression beds with a few simple clicks.  Ideally this type of end user self-service should also be available in your local integration bus as a general capability… Replacing the basic relatively unpolished version offered here.</li>
          </ul>
        </div>
        <div class="row column top-margin-small">
          <h2>Limitations</h2>
          <ul>
            <li><strong>VBA macros:</strong> While almost all excel functions and mechanics are supported, VBA macros are not. You can have them in your file and as long as you don't need then for the calculation you are exposing (e.g. you use them for formatting or test case generation).  A simple way to test this is to disable macros and see if your calculation works in native excel.  If you need a particular function to support your work this can be easily supported but will take a day or two of coding.</li>
            <li><strong>External references:</strong> The purpose of these engines us that they are atomic and immutable over time (i.e. if you to go back 3 years later to prove how you arrived at a result and recreate it exactly - you can).  External references break this.  Support could be developed by enabling and tracking relationships between engines and reference tables with relatively little effort, but this is beyond the scope of this POC.  For now please embed external tables / make sure your calculation works if you click “no” to any update external references questions when you load your spreadsheet in Excel.</li>
            <li><strong>No saving of logs or engines:</strong> In this POC we only store information in memory.  You should expect to lose any information you upload without warning.  Technically this is trivial to change though.</li>
            <li><strong>Named table references:</strong> if you have created named tables and use them in formulas (e.g. ‘Table1[#All]’) then these will not resolve.  If you replace them with the corresponding range reference (e.g. ‘Sheet1!$A$14:$C$17’) then everything will work. </li>
          </ul>
        </div>
        <div class="row column top-margin-small">
          <h2>Security</h2>
          <ul>
            <li style="list-style-type:none;">
            For the purposes of this POC we have disabled the ability download Excel files that have been uploaded (you can still download the examples we have provided and pre-loaded into memory as part base install).  That being said, you should still not upload files containing confidential information.  Also, note that it is possible to view the logs of test calls made. You should still not include any customer identifiable information in the calls (insurance calculators should not require this in any case.)
            </li>
          </ul>
        </div>
        <div class="row column top-margin-small">
          <h2>Performance</h2>
          <ul>
            <li>Performance of the POC is quite good.  Even heavy Excel files (10 tabEven heavy Excel files (10 tabs, 40 step calculations, dozens of inputs and outputs) should be able to comfortably handle 20-100 transactions per second per processing core.  This could further be optimized with caching, but realistically, marshalling and network latency will become the limiting factor.  In batch execution scenarios we have found full calculations to take between 5 and 20 milliseconds on a laptop.</li>
            <li>Calculation engines (after loading the internal object model) take up 100k to a few megabytes. Coherently single threaded and, at present, the POC only constructs a single engine per excel model. Thread safety is enforced but this means you really only get to use one core at a time per engine in the POC. Engine thread pooling and IO caching would further improve scalability (estimate 5 times improvement on a typical production sized machine).</li>
            <li>If you would like to explore multi-core/could performance using the POC please upload slightly different versions of the same excel calculation (just change one cell anywhere in the file to have it recognized as a different engine) and test across the variations concurrently.</li>
          </ul>
        </div>
      </div>
    </div>
  </body>


</asp:Content>
