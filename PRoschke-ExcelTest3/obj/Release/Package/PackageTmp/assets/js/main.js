var uploadfile = false,
	runtestfile = false,
	degree_range = [
		{"min":0,"max":1,"degree":{"start":-99,"end":-69}},
		{"min":1,"max":5,"degree":{"start":-69,"end":-43}},
		{"min":5,"max":10,"degree":{"start":-43,"end":-18}},
		{"min":10,"max":20,"degree":{"start":-18,"end":21}},
		{"min":20,"max":30,"degree":{"start":21,"end":62}},
		{"min":30,"max":50,"degree":{"start":62,"end":90}},
		{"min":50,"max":75,"degree":{"start":90,"end":114}},
		{"min":75,"max":100,"degree":{"start":114,"end":143}}
	];

$(function() {
	//preventing the default action for both the "drop" and "dragover" events
	$(document).bind('drop dragover', function (e) {
	    e.preventDefault();
	});

	//event handling
	$('body').on('click', '#upload_button', function(e) {
		openPopup('upload');
	});
	$('body').on('click', '#run_test_button', function(e) {
		openPopup('run_test');
	});
	$('body').on('click', '#overlay-background .pop-up-close', function(e) {
		closePopup();
	});
	$('body').on('drop', '#upload_content .dragdopbox', function (e) {
		if(e.originalEvent.dataTransfer.files.length != 0){
			uploadfile = e.originalEvent.dataTransfer.files[0];
			showFile('upload_content', uploadfile);
		}
	});
	$('body').on('change', '#fileupload', function(e) {
		if(e.target.files.length != 0){
			uploadfile = e.target.files[0];
			showFile('upload_content', uploadfile);
		}
	});
	$('body').on('drop', '#run_test_content .dragdopbox', function (e) {
		if(e.originalEvent.dataTransfer.files.length != 0){
			runtestfile = e.originalEvent.dataTransfer.files[0];
			showFile('run_test_content', runtestfile);
		}
	});
	$('body').on('change', '#fileruntest', function(e) {
		if(e.target.files.length != 0){
			runtestfile = e.target.files[0];
			showFile('run_test_content', runtestfile);
		}
	});
	$('body').on('click', '#upload_content .submit_button button', function(e) {
		if(uploadfile){
			upload("./", uploadfile);
		}
	});
	$('body').on('click', '#run_test_content .submit_button button', function(e) {
		if(runtestfile){
			upload("./TestTester", runtestfile);
		}
	});
	$('body').on('click', '#upload_content .remove_button', function(e) {
		uploadfile = false;
		removeFile('upload_content');
	});
	$('body').on('click', '#run_test_content .remove_button', function(e) {
		runtestfile = false;
		removeFile('run_test_content');
	});
	$('body').on('click', '#json_test_button', function(e) {
		openPopup("");
	});
	$('body').on('click', '.show-item i', function(e) {
		var target = $(this).parent().parent().parent().parent();
		toggleContent(target);
	});
	$(".comment").mouseover(function() {
		var comment_hover = $(this).children(".comment_hover");
		var comment_height = comment_hover.height()-5;
		comment_hover.css({"top":"-"+comment_height+"px"});
		comment_hover.show();
	}).mouseout(function() {
		$(this).children(".comment_hover").hide();
	});

	//init datepicker
	$(".datepicker").datepicker({
		dateFormat: "dd-mm-yy"
	});
});

function openPopup(target){
	$("#popup_container").removeClass("hide");
	if(target != ""){
		$("#"+target+"_content").removeClass("hide");
	}
}

function closePopup(){
	$("#popup_container").addClass("hide");
	$(".content_container").addClass("hide");
}

function showFile(target_id, selectedFile){
	$("#"+target_id+" .upload_text").addClass("hide");

	$("#"+target_id+" .non_upload_text .file_name").html(selectedFile.name);
	$("#"+target_id+" .non_upload_text").removeClass("hide");
}

function removeFile(target_id){
	$("#"+target_id+" .non_upload_text .file_name").html("");
	$("#"+target_id+" .non_upload_text").addClass("hide");

	$("#"+target_id+" .upload_text").removeClass("hide");
}

function upload(url, file){
	var data = new FormData();
	data.append("file", file);

	// ajax request
	var ajax = new XMLHttpRequest();
	ajax.open( "POST", url, true );
	ajax.onload = function()
	{
		if( ajax.status >= 200 && ajax.status < 400 ){
			var responseText = JSON.parse( ajax.responseText );
			console.log(responseText);
		}else{
			alert( 'Error. Please, contact the webmaster!' );
		}
	};
	ajax.onerror = function()
	{
		alert( 'Error. Please, try again!' );
	};

	console.log(data.get("file"));
	ajax.send(data);
}

function toggleContent(target){
	var target_head = target.find(".show-item i");
	var target_content = target.find(".full_content");

	if(target_content.css("display") == "none"){
		if(target.attr("id") == "log_container"){
			$(".speed-dashboard-point").rotate(-99);
		}

		target_content.slideDown(function(){
			if(target.attr("id") == "log_container"){
				var time = Number($("#time_spend").html());
	            $('#time_spend').each(function () {
					var $this = $(this);
					jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
						duration: 1500,
						easing: 'swing',
						step: function () {
							$this.text(this.Counter.toFixed(2));
						}
					});
				});
				$(".speed-dashboard-point").rotate({
					duration: 1500,
					angle: -99,
					animateTo: calTimeDeg(time)
				});
			}

			target_head.removeClass("icon-down-open-big").addClass("icon-up-open-big");
		});

		var timer = 500;
	    $('html, body').stop().animate({scrollTop:target.offset().top}, timer);
	}else{
		target_content.slideUp(function(){
			target_head.removeClass("icon-up-open-big").addClass("icon-down-open-big");
		});
	}
}

function calTimeDeg(minisec){
	var range;
	if(minisec < 1){
		range = degree_range[0];
	}else if(minisec < 5){
		range = degree_range[1];
	}else if(minisec < 10){
		range = degree_range[2];
	}else if(minisec < 20){
		range = degree_range[3];
	}else if(minisec < 30){
		range = degree_range[4];
	}else if(minisec < 50){
		range = degree_range[5];
	}else if(minisec < 75){
		range = degree_range[6];
	}else if(minisec < 100){
		range = degree_range[7];
	}else{
		range = degree_range[7];

		return range.degree.end;
	}

	var min = range.min,
		max = range.max,
		start = range.degree.start,
		end = range.degree.end;

	var time_slot = max-min,
		degree_slot = end-start,
		time_increase = minisec-min;

	return start+(degree_slot*(time_increase/time_slot));
}