﻿<%@ Page Title="Run Test Set" Language="C#" MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="TestTester.aspx.cs" Inherits="PRoschke_ExcelTest3.TestTester" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Please upload a test set</h2>
    <p>If you don't have one you can get a template from engines UI Test page or dowload a template with a pre-poplulate test set from the engine's log page.</p>
    <p>&nbsp;</p>
   <Table>
         <tr>
             <td><asp:FileUpload ID="FileUpload1" runat="server" /></td>
             <td><asp:Button ID="btnUpload" runat="server" Text="Upload and Run" OnClick="btnUpload_Click" /></td>
         </tr>
    </Table>
        
        
    <p>&nbsp;</p>


</asp:Content>