﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace PRoschke_ExcelTest3
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // Add Routes.
            RegisterCustomRoutes(RouteTable.Routes);

            // Create an application level log list for calc calls
            List<XLog> XLogs = new List<XLog>();
            Application.Add("XLogs", XLogs);




            // Create applicaiton level opject for storing the Excel calc engines.  Prepopulate any from ~/files folder.
            List<XLObj> XLObjs = new List<XLObj>();
            Application.Add("XLObjs", XLObjs);
            


            // See what's in the files folder
            foreach (string fn in Directory.EnumerateFiles(Server.MapPath("~/files/")))
            {
                if (Path.GetFileName(fn).Equals("TestResultTemplate.xlsx"))
                {
                    //App variable for holding the default test result template from  from ~/files/TestResultTemplate.xlsx folder.
                    XLTest XLT = XLT = new XLTest(Server.MapPath("~/files/" + Path.GetFileName(fn)));
                    Application.Add("XTestResultTemplate", XLT); 
                }
                else  // We are dealing with a Test Engine
                {
                    XLObj XLO = new XLObj();
                    XLO.LoadFromFile(Server.MapPath("~/files/" + Path.GetFileName(fn)));
                    XLObjs.Add(XLO);
                }
            }
        }

        void RegisterCustomRoutes(RouteCollection routes)
        {
            // Custom URL handling rules for our pages

            //For Viewing Excel files
            routes.MapPageRoute("GetSourceFile", "{hash}/GetSourceFile", "~/GetSourceFile.aspx");

            //For Getting Corresponding Test Bed files
            routes.MapPageRoute("GetTestSetTemplate", "{hash}/GetTestSetTemplate", "~/GetTestSetTemplate.aspx");

            //For viewing and filtering logs
            routes.MapPageRoute("GetLogs", "{hash}/GetLogs", "~/GetLogs.aspx");

            //For getting an excel test set file from a filtered set of logs
            routes.MapPageRoute("GetLogTestbed", "{hash}/GetLogTestbed", "~/GetLogTestbed.aspx");

            //UITester
            routes.MapPageRoute("UITestRoute", "{hash}/UITest", "~/UITest2.aspx");
 
            //JSON Tester
            routes.MapPageRoute("JSONPoster", "{hash}/JSONPoster", "~/JSONPoster.aspx");
        }
    }
}
