﻿<%@ Page Title="Get Logs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GetLogs.aspx.cs" Inherits="PRoschke_ExcelTest3.GetLogs" %>

<asp:Content ID="Head" ContentPlaceHolderID="head" runat="server">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Rosetta Portal - Calculation</title>
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="favicon.ico" title="axahongkong-favicon" rel="icon">
        <link rel="stylesheet" href="assets/Icon/css/fontello.css">
        <link rel="stylesheet" href="assets/foundation/css/foundation.css">
        <link rel="stylesheet" href="assets/jquery-ui/jquery-ui.css">
        <script src="https://use.typekit.net/rac8rmf.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script> 
</asp:Content>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">



        <body>
        <div class="body-wrap">
            <div class="box-full-width">
                <nav aria-label="You are here:" role="navigation">
                    <ul class="breadcrumbs">
                        <li><a href="#">Dashbaord</a></li>
                        <li><a href="#">Testfile-NEW.xlsx</a></li></li>
                        <li><span class="show-for-sr">Current: </span> Full Logs</li>
                    </ul>
                </nav> 
            </div>
        </div>
        <div class="body-wrap">
            <div class="box-full-width">
                <div class="content-header bottom-margin">
                    <div class="content-header-left">
                        <h1>See Full Logs</h1>
                    </div>
                    <div class="content-header-right">
                        <%--<button type="button" class="button large light-pink-bg"><i class="icon-template"></i> Generate HTML</button>--%>
                        <%--<button type="button" class="button large blue-bg"><i class="icon-excel"></i> Generate Excel</button>--%>
<%--                        <asp:LinkButton ID="ImageButton2" runat="server" OnClick="btnHTML_Click" CssClass="button light-pink-bg" Height="16px" Width="101px">
                            <i class="icon-template"></i>Generate HTML

                        </asp:LinkButton>--%>
                        
                        <asp:LinkButton ID="ImageButton3" runat="server" OnClick="btnExcel_Click" CssClass="button large blue-bg">
                                <i class="icon-excel"></i>Generate Excel
                        </asp:LinkButton>
                        <%--<button type="button" class="button large green-bg"><i class="icon-json"></i> Generate JSON</button>--%>
                        <asp:LinkButton ID="ImageButton4" runat="server" OnClick="btnJSON_Click" CssClass="button large green-bg">
                                <i class="icon-json"></i>Generate Json
                        </asp:LinkButton>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnHTML_Click" CssClass="button large light-pink-bg">
                                <i class="icon-template"></i> Generate HTML

                        </asp:LinkButton>
                    </div>
                </div>
<%--                <div class="row ">    
                    <img src="1.PNG">
                    <img src="2.PNG">
                </div>--%>
            </div>
        </div>
    </body>





    <%--To be migrated--%>
    <%--<h2><%: Title %></h2>--%>

    <p><asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="True" Font-Names="Arial" ForeColor="#CC0000"></asp:Label>

    <%--</p>--%>

            <%--<asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" />--%>
    <table>
        <tr>
            <td>Service Name</td>
            <td>Service Revision</td>
            <td>Input Filter</td>
            <td>Output Filter</td>
            <td>Error Message Filter</td>
            <td>Log Message Filter</td>

        </tr>
            <td><asp:TextBox Width="200" ID="tbServiceName" runat="server"></asp:TextBox></td>
            <td><asp:TextBox Width="200" ID="tbServiceRevision" runat="server"></asp:TextBox></td>
            <td><asp:TextBox Width="200" ID="tbInputFilter" runat="server"></asp:TextBox></td>
            <td><asp:TextBox Width="200" ID="tbOutputFilter" runat="server"></asp:TextBox></td>
            <td><asp:TextBox Width="200" ID="tbErrorMessageFilter" runat="server"></asp:TextBox></td>
            <td><asp:TextBox Width="200" ID="tbLogMessageFilter" runat="server"></asp:TextBox></td>

        </tr>
    </table>
<%--    <p></p>
    
     <asp:Button ID="btnHTML" runat="server" Text="Filter and Generate HTML Summary" OnClick="btnHTML_Click" />
     &nbsp;
     <asp:Button ID="btnExcel" runat="server" Text="Filter and Generate Excel Test Bed" OnClick="btnExcel_Click" />
     &nbsp;
     <asp:Button ID="btnJSON" runat="server" Text="Filter and Generate Full JSON" OnClick="btnJSON_Click" />

    <p></p>--%>
    <hr />
       
    <table border="1">
       <asp:Literal ID="ltLog" runat="server" ViewStateMode="Disabled"></asp:Literal>
    </table>


    <p></p>

</asp:Content>
