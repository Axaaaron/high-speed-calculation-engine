﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace PRoschke_ExcelTest3
{
    /// <summary>
    /// Example route handler... could be useful but it's a pain to get Routeparameters here.  See https://weblog.west-wind.com/posts/2011/mar/28/custom-aspnet-routing-to-an-httphandler
    /// </summary>
    public class RouteHandler : System.Web.Routing.IRouteHandler
    {
        public IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext)
        {
            HttpHanler httpHandler = new HttpHanler();
            return httpHandler;
        }
        public class HttpHanler : IHttpHandler
        {

            public bool IsReusable
            {
                get
                {
                    return false;
                }
            }

            public void ProcessRequest(HttpContext context)
            {
                // Hah - can't easily get a routedata here sooo going back to using an ASPX
                XLObj XLO = (XLObj)context.Application["CurXLObj"];
                context.Response.ContentType = XLO.MimeType;
                context.Response.AppendHeader("content-disposition", "attachment; filename=\"" + XLO.FileName + "\"");
                context.Response.BinaryWrite(XLO.Data);

            }
        }
    }
}