﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PRoschke_ExcelTest3
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FileGV.DataSource = (List<XLObj>)Application["XLObjs"];
            FileGV.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            HttpPostedFile hpf = Request.Files[0];
            //check file was submitted
            if (hpf != null && hpf.ContentLength > 0)
            {
                XLObj XLO = new XLObj()
                {
                    FileName = Regex.Match(hpf.FileName, @"(/|\\)?(?<fileName>[^(/|\\)]+)$").Groups["fileName"].ToString(),   // to trim off whole path from browsers like IE
                    MimeType = hpf.ContentType,
                    Data = Helper.ReadFully(hpf.InputStream, 0)
                };
                // check to see if same engine signature already in memory, otherwise add to list
                List<XLObj> XLObjs = (List<XLObj>)Application.Get("XLObjs");
                if (XLObjs.Find(x => x.Hash == XLO.Hash) == null) XLObjs.Add(XLO);

                //TODO redirect to UITest page after upload
                Response.Redirect("/" + XLO.Hash + "/UITest");

            }
        }


        protected void FileGV_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }


    }
}